====
Grab
====

Grab is python site scraping framework. Grab provides interface to pycurl and lxml libraries.


Documentation
=============

Documentation (Russian): http://grablib.org/docs/

Documentation (English): not yet ready, please use google translate

Please submit all questions to google group: http://groups.google.com/group/python-grab/


Dependencies
============

* lxml
* pycurl
