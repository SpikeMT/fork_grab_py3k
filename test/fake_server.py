from unittest import TestCase
try:
	from urllib import urlopen
except ImportError:
	from urllib.request import urlopen

from util import FakeServerThread, RESPONSE, BASE_URL, REQUEST

class TestFakeServer(TestCase):
    def setUp(self):
        FakeServerThread().start()

    def test_get(self):
        RESPONSE['get'] = 'zorro'.encode('utf8')
        data = urlopen(BASE_URL).read()
        self.assertEqual(data, RESPONSE['get'])

    def test_path(self):
        urlopen(BASE_URL + '/foo').read()
        self.assertEqual(REQUEST['path'], '/foo')

        urlopen(BASE_URL + '/foo?bar=1').read()
        self.assertEqual(REQUEST['path'], '/foo?bar=1')


    def test_post(self):
        RESPONSE['post'] = 'foo'.encode('utf8')
        data = urlopen(BASE_URL, 'THE POST'.encode('utf8')).read()
        self.assertEqual(data, RESPONSE['post'])
