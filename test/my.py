# coding: utf-8

#==========WORKED=============
def Text_Tools():
    import text_tools
    aaa = text_tools.TextExtensionTest()
    aaa.test_find_number()
    aaa.test_drop_space()
    aaa.test_normalize_space()

def Tools_Control():
    import tools_control
    aaa = tools_control.ControlToolsTestCase()
    aaa.test_sleep()

def FakeServer():
    from fake_server import TestFakeServer
    a = TestFakeServer()
    a.setUp()
    a.test_get()
    a.test_path()
    a.test_post()
    
def Extension():
    import extension
    #a = extension.ExtensionA()
    #a.extra_foo()
    #b = extension.ExtensionB()
    #b.extra_foo()
    #c = extension.Worker()
    #c.do_something()
    d = extension.ExtensionTestCase()
    d.setUp()
    d.test_extensions()

def ToolsAccount():
    import tools_account
    tools_account.TestAccount().test_misc()
    
#==========CRASHED=============
def Grab_Pickle():
    import grab_pickle
    aaa = grab_pickle.TestGrab()
    aaa.setUp()
    aaa.test_pickling()

def PyqueryExtension():
    import pyquery_extension
    a = pyquery_extension.PyqueryExtensionTest()
    a.setUp()
    a.test_some_things()

    

if __name__ == "__main__":
    #Text_Tools()
    #print("Text_Tools")
    #Tools_Control()
    #print("Tools_Control")
    #FakeServer()
    #print("FakeServer")
    #Extension()
    #print("Extension")
    
    ToolsAccount()
    print("ToolsAccount")
    
    
    
    #Grab_Pickle()
    #print("Grab_Pickle")
    
    #PyqueryExtension()
    #print("PyqueryExtension")
    
    
    