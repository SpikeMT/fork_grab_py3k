.. redirect-docs documentation master file, created by
   sphinx-quickstart on Tue Jun 19 23:05:19 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Grab - site scraping framework
==============================

.. warning::

    This is outdated document. You can get actual Grab documentation at official Grab website: http://grablib.org/docs

Useful links
------------

:official site: http://grablib.org
:repo & bugtracker: http://bitbucket.org/lorien/grab
:documentation: http://grablib.org/docs
:jabber conference: grablab@conference.jabber.ru
:mail list: http://groups.google.com/group/python-grab
:email conact: lorien@lorien.name

.. toctree::
   :maxdepth: 2
